# Calculator

The Calculator is a web-based calculator application with extended functionality, incorporating basic arithmetic operations as well as square root (√) and power (^) functions. This project is built using HTML, CSS, and JavaScript.

## Features

- Basic Arithmetic Operations:
  - Addition (+)
  - Subtraction (-)
  - Multiplication (*)
  - Division (/)

- Functions:
  - Square Root (√)
  - Power (^)

## Project Structure

- **index.html:** The main HTML file that defines the structure of the calculator interface.
- **style.css:** The CSS file containing styles for the calculator layout and buttons.
- **script.js:** The JavaScript file handling the calculator's logic and functionality.

## HTML Structure

- The calculator interface consists of an input field (`<input type="text" id="display" readonly>`) to display expressions and results.
- Buttons for digits, arithmetic operations, and complex functions are organized within a `<div class="buttons">`.

## CSS Styles

- The CSS file (`style.css`) provides styles for the calculator layout, buttons, and responsiveness.
- Styles include button formatting, grid layout for buttons, and responsive design for various screen sizes.

## JavaScript Logic

- The JavaScript file (`script.js`) contains the logic for the calculator.
- Functions:
  - `appendToDisplay(value)`: Appends the given value to the display.
  - `clearDisplay()`: Clears the display.
  - `calculateResult()`: Evaluates the expression in the display and displays the result.
- Complex Functions Handling:
  - The `calculateResult()` function preprocesses the expression by replacing 'sqrt(' with 'Math.sqrt(' and '^' with '**' for proper JavaScript evaluation.

## Usage

1. Open the `index.html` file in a web browser.
2. Use the calculator interface to input expressions.
3. Press the "=" button to calculate the result.
4. Use the "C" button to clear the display.

## Notes

- The calculator uses the `eval()` function for simplicity. In a production environment, consider implementing a safer method for evaluating expressions.
- Complex functions are processed before evaluation to ensure compatibility with JavaScript syntax.

## Conclusion

The Calculator project provides a basic calculator with additional features for square root and power operations. It serves as an educational example and can be enhanced with additional functionality and improved user experience.

Feel free to explore, modify, and contribute to this project!
