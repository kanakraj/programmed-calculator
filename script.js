let display = document.getElementById('display');

function appendToDisplay(value) {
    display.value += value;
}

function clearDisplay() {
    display.value = '';
}

function calculateResult() {
    try {
        // Replace 'sqrt(' with 'Math.sqrt(' and '^' with '**' for JavaScript evaluation
        let expression = display.value.replace(/sqrt\(/g, 'Math.sqrt(').replace(/\^/g, '**');
        display.value = eval(expression);
    } catch (error) {
        display.value = 'Error';
    }
}
