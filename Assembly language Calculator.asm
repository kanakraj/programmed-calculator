section .data
    msg db 'Enter first number: ', 0
    msg_len equ $ - msg

    msg2 db 'Enter second number: ', 0
    msg2_len equ $ - msg2

    result_msg db 'Result: ', 0
    result_msg_len equ $ - result_msg

section .bss
    num1 resb 10
    num2 resb 10
    result resb 10

section .text
    global _start

_start:
    ; Display message for the first number
    mov eax, 4
    mov ebx, 1
    mov ecx, msg
    mov edx, msg_len
    int 0x80

    ; Read the first number
    mov eax, 3
    mov ebx, 0
    mov ecx, num1
    mov edx, 10
    int 0x80

    ; Convert the first number to integer
    mov eax, num1
    call str2int
    mov ebx, eax

    ; Display message for the second number
    mov eax, 4
    mov ebx, 1
    mov ecx, msg2
    mov edx, msg2_len
    int 0x80

    ; Read the second number
    mov eax, 3
    mov ebx, 0
    mov ecx, num2
    mov edx, 10
    int 0x80

    ; Convert the second number to integer
    mov eax, num2
    call str2int

    ; Check for division by zero
    cmp eax, 0
    je divide_by_zero

    ; Perform addition
    add ebx, eax
    jmp display_result

divide_by_zero:
    ; Display error message for division by zero
    mov eax, 4
    mov ebx, 1
    mov ecx, error_msg
    mov edx, error_msg_len
    int 0x80
    jmp exit_program

display_result:
    ; Display result message
    mov eax, 4
    mov ebx, 1
    mov ecx, result_msg
    mov edx, result_msg_len
    int 0x80

    ; Convert result to string
    mov eax, ebx
    mov ebx, result
    call int2str

    ; Display the result
    mov eax, 4
    mov ebx, 1
    mov ecx, result
    int 0x80

exit_program:
    ; Exit the program
    mov eax, 1
    xor ebx, ebx
    int 0x80

str2int:
    ; Convert string to integer
    xor eax, eax
    xor edi, edi
.next_digit:
    movzx edx, byte [eax]
    cmp edx, 0
    je .done
    sub edx, '0'
    imul edi, edi, 10
    add edi, edx
    inc eax
    jmp .next_digit
.done:
    mov eax, edi
    ret

int2str:
    ; Convert integer to string
    mov ecx, 10
    mov edi, 0
    .repeat:
        dec ecx
        xor edx, edx
        div ecx
        add dl, '0'
        mov [ebx + edi], dl
        inc edi
        test eax, eax
        jnz .repeat
    mov byte [ebx + edi], 0
    ret

section .data
    error_msg db 'Error: Division by zero', 0
    error_msg_len equ $ - error_msg
