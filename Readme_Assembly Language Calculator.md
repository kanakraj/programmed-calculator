# Assembly Language Calculator

A basic calculator implemented in assembly language that performs simple arithmetic operations written for x86 architecture, using  NASM (Netwide Assembler) syntax

## Features

- **User Input:**
  - Accepts input for two numbers and the desired arithmetic operation.

- **Arithmetic Operations:**
  - Implements addition, subtraction, multiplication, and division.

- **Output Result:**
  - Displays the result of the arithmetic operation.

- **Error Handling:**
  - Includes basic error handling, such as division by zero.

## Implementation

### User Input
Use system calls or interrupt instructions to read user input for two numbers and the desired operation.

### Arithmetic Operations
Write separate functions for each arithmetic operation. Utilize registers for calculations.

### Control Flow
Implement control flow instructions to select the appropriate arithmetic operation based on user input.

### Output Result
Display the result using system calls or interrupts.

### Error Handling
Check for errors such as division by zero and display appropriate messages.

